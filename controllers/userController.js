require('dotenv').config();
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { User } = require('../models/users');

const registerUser = (req, res) => {
    const { name, username, password } = req.body;
    const user = new User({
        name,
        username,
        password: bcrypt.hashSync(password, 10),
        createdDate: new Date().toISOString()
    });

    user.save()
        .then(() => res.status(200).json({ message: 'Success' }))
        .catch(() => {
            return res.status(400).send({ message: 'string' });
        });
};

const loginUser = async (req, res) => {
    const user = await User.findOne({ username: req.body.username });
    if (user && await bcrypt.compare(String(req.body.password), String(user.password))) {
        const payload = { username: user.username, name: user.name, userId: user.id };
        const jwtToken = jwt.sign(payload, process.env.SECRET_KEY);
        return res.status(200).json({ message: 'Success', jwt_token: jwtToken });
    } else {
        return res.status(400).json({ message: 'Not authorized' });
    }
};

const getProfileInfo = async (req, res) => {
    try {
        const user = await User.findById({ _id: req.user.userId });
        return res.status(200).send({
            user: {
                _id: user.id,
                username: user.username,
                createdDate: user.createdDate
            },
        });
    } catch (err) {
        return res.status(400).json({ message: err.message });
    }
};

const deleteUser = async (req, res) => {
    try {
        await User.findByIdAndDelete({ _id: req.user.userId })
            .then(() => res.status(200).json({ message: 'success' }));
    } catch (err) {
        return res.status(400).json({ message: err.message });
    }
};

const changePass = (req, res) => {
    try {
        User.findOne({ _id: req.user.userId })
            .then((user) => {
                bcrypt.compare(String(req.body.oldPassword), user.password)
                    .then(async (result) => {
                        if (result) {
                            user.password = await bcrypt.hash(req.body.newPassword, 10);
                            user.save();
                            return res.status(200).json({ message: 'success' });
                        }
                        return res.status(400).json({ message: 'Wrong password' });
                    });
            });
    } catch (err) {
        return res.status(400).json({ message: err.message });
    }
};

module.exports = {
    registerUser,
    loginUser,
    getProfileInfo,
    deleteUser,
    changePass,
};
