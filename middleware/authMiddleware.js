require('dotenv').config();
const jwt = require('jsonwebtoken');

const authMiddleware = (req, res, next) => {
    const { authorization } = req.headers;

    if (!authorization) {
        return res.status(401).json({ message: 'Please, provide authorization header' });
    }

    const [, token] = authorization.split(' ');

    if (!token) {
        return res.status(401).json({ message: 'Please, include token to request' });
    }

    try {
        const payload = jwt.verify(token, process.env.SECRET_KEY);
        req.user = {
            userId: payload.userId,
            username: payload.username,
            name: payload.name,
        };
        return next();
    } catch (error) {
        return res.status(401).json({ message: error.message });
    }
};

module.exports = {
    authMiddleware,
};
