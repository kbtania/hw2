const express = require('express');
const morgan = require('morgan');
const fs = require('fs');

const app = express();
const mongoose = require('mongoose');

const { authMiddleware } = require('./middleware/authMiddleware');
const { errorHandler } = require('./helpers/errorHandler');

const DB_CONNECTION = 'mongodb+srv://hw2:6IKuiXqSchLIla5K@cluster0.6asexxf.mongodb.net/?retryWrites=true&w=majority'
const PORT = 8080

const accessLogStream = fs.createWriteStream('access.log', { flags: 'a' });

mongoose.connect(DB_CONNECTION);

const { usersRouter } = require('./routes/usersRouter');
const { notesRouter } = require('./routes/notesRouter');

app.use(express.json());
app.use(morgan('combined', { stream: accessLogStream }));

app.use('/api/auth', usersRouter);
app.use('/api/users', usersRouter);
app.use('/api/notes', authMiddleware, notesRouter);

const start = async () => {
    try {
        if (!fs.existsSync('files')) {
            fs.mkdirSync('files');
        }
        app.listen(PORT);
    } catch (error) {
        console.error(`Server error: ${error.message}`);
    }
};

start();

app.use(errorHandler);
