const express = require('express');

const router = express.Router();
const {
    createNote,
    getNote,
    deleteNoteById,
    getProfileInfoNotes,
    updateNoteById,
    markNoteCompletedById
} = require('../controllers/notesController');

router.post('/', createNote);
router.get('/:id', getNote);
router.delete('/:id', deleteNoteById);
router.get('/', getProfileInfoNotes);
router.put('/:id', updateNoteById);
router.patch('/:id', markNoteCompletedById);

module.exports = {
    notesRouter: router,
};
