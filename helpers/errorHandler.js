function errorHandler(err, req, res) {
    console.error(err);
    return res.status(500).send({ message: 'String' });
}

module.exports = {
    errorHandler,
};